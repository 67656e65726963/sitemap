<?php

/**
 * Plugin Name:       Toy Sitemap
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-sitemap
 * Description:       Helps generate elementary XML sitemaps.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! function_exists( 'toy_sitemap' ) ) :
	function toy_sitemap() {
		$type = array( 'post', 'page' );
		$from = array(
			'numberposts' => -1,
			'order'       => 'DESC',
			'orderby'     => 'modified',
			'post_type'   => $type,
		);

		$list = get_posts( $from );
		$path = get_home_path() . 'sitemap.xml';

		$file = new DomDocument();
		$tree = new SimpleXMLElement( '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" />' );

		foreach ( $list as $item ) :
			setup_postdata( $item );

			$link = get_permalink( $item->ID );

			$date = explode( ' ', $item->post_modified );
			$date = array_shift( $date );

			$node = $tree->addChild( 'url' );

			$node->addChild( 'changefreq', 'monthly' );
			$node->addChild( 'loc', $link );
			$node->addChild( 'lastmod', $date );
			$node->addChild( 'priority', '1.0' );
		endforeach;

		$data = $tree->asXML();

		$file->loadXML( $data );
		$file->formatOutput       = true;
		$file->preserveWhiteSpace = false;
		$file->save( $path );
	}
endif;

add_action( 'save_post', 'toy_sitemap', 67 );
add_action( 'save_page', 'toy_sitemap', 67 );
